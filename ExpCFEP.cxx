#include "ExpCFEP.h"

ExpCFEP::ExpCFEP(const char *title, int bineta, int binphi)
{
  isoff = false;
  MakeHistos(title, bineta, binphi);
}

ExpCFEP::~ExpCFEP()
{
  if (cnump) delete cnump;
  if (cdenp) delete cdenp;
}
  
void ExpCFEP::SetOff(Bool_t dooff)
{
  isoff = dooff;
}

void ExpCFEP::MakeHistos(const char *title, int bineta, int binphi)
{
  char htitle[2000];

  sprintf(htitle, "cnumep%s", title);
  cnump = new TH2D(htitle, htitle, binphi, -TMath::Pi()/2.0, 3*TMath::Pi()/2.0, bineta, -4.0, 4.0);
  sprintf(htitle, "cdenep%s", title);
  cdenp = new TH2D(htitle, htitle, binphi, -TMath::Pi()/2.0, 3*TMath::Pi()/2.0, bineta, -4.0, 4.0);

  cnump->Sumw2();
  cdenp->Sumw2();
}


void ExpCFEP::AddRealPair(double deta, double dphi, double weight)
{
  if (isoff) return;

  cnump->Fill(dphi, deta, weight);
}

void ExpCFEP::AddMixedPair(double deta, double dphi, double weight)
{
  if (isoff) return;

  cdenp->Fill(dphi, deta, weight);
}

void ExpCFEP::Write()
{
  if (isoff) return;

  cnump->Write();
  cdenp->Write();
}

